terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
    region =  var.region
    access_key = var.access_key
    secret_key = var.secret_key

}

resource "aws_vpc" "ismail-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name : "ismail-vpc"

    }
    
}

resource "aws_subnet" "ismail-1-subnet" {
     vpc_id = aws_vpc.ismail-vpc.id
     cidr_block = var.public_subnet_cidr_blocks[0]
     availability_zone = var.avail_zone[0]
     tags = {
        Name: "ismail-subnet1"
     }
}

resource "aws_subnet" "ismail-2-subnet" {
     vpc_id = aws_vpc.ismail-vpc.id
     cidr_block = var.public_subnet_cidr_blocks[1]
     availability_zone = var.avail_zone[1]
     tags = {
        Name: "ismail-subnet2"
     }
}

resource "aws_route_table" "ismail-rtb" {
    vpc_id = aws_vpc.ismail-vpc.id

    route {
            cidr_block = "0.0.0.0/0"
            gateway_id = aws_internet_gateway.ismail-igw.id
        }
    tags = {
            Name: "ismail-rtb"
        }
}

resource "aws_internet_gateway" "ismail-igw" {
    vpc_id = aws_vpc.ismail-vpc.id
    tags = {
    Name: "ismail-igw"
  }
}

resource "aws_route_table_association" "ismail-rtb-ass" {

    subnet_id = aws_subnet.ismail-1-subnet.id
    route_table_id = aws_route_table.ismail-rtb.id
   
}

resource "aws_route_table_association" "ismail-rtb-ass2" {
    
    subnet_id = aws_subnet.ismail-2-subnet.id
    route_table_id = aws_route_table.ismail-rtb.id
   
}

/*resource "aws_default_security_group" "ismail-sg" {
    
    tags = {
        Name: "ismail-sg"
    }
    vpc_id = aws_vpc.ismail-vpc.id

    ingress{
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["102.88.34.60/32"]
    }
     ingress{
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

     egress{
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []
    }
}
*/
resource "aws_network_acl" "ismail-nacl" {
    
    tags = {
        Name: "ismail-nacl"
    }
    vpc_id = aws_vpc.ismail-vpc.id
    subnet_ids = [aws_subnet.ismail-1-subnet.id , aws_subnet.ismail-2-subnet.id]

    ingress{
        action = "allow"
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_block = "0.0.0.0/0"
        rule_no = 100
    }
  

     egress{
        action = "allow"
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_block = "0.0.0.0/0"
        rule_no = 100
       
    }
}

resource "aws_security_group" "ismail-lb-sg" {
    name = "ismail-lb-sg"
    description = "security group for ismail load balancer"
    tags = {
        Name: "ismail-lb-sg"
    }
    vpc_id = aws_vpc.ismail-vpc.id
    
    ingress{
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
     
    }
    ingress{
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
     
    }
  

    egress{
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
       
    }
}

resource "aws_security_group" "ismail-ec2-sg" {
    name = "ismail-ec2-sg"
    description = "security group for ismail ec2 instances,ssh,http and https"
    tags = {
        Name: "ismail-ec2-sg"
    }
    vpc_id = aws_vpc.ismail-vpc.id
    
    ingress{
        description = "ssh"
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
       
     
    }
    ingress{
        description = "http"
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        security_groups = [aws_security_group.ismail-lb-sg.id]
     
    }
    ingress{
        description = "https"
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        security_groups = [aws_security_group.ismail-lb-sg.id]
     
    }
  

    egress{
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
       
    }
}


data "aws_ami" "ubuntu" {
        most_recent = true

        filter {
            name   = "name"
            values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
        }

        filter {
            name   = "architecture"
            values = ["x86_64"]
        }

        filter {
            name   = "virtualization-type"
            values = ["hvm"]
        }

        owners = ["099720109477"] # Canonical
        }

#create a key pair to enable ssh access


#create the 3 instances
resource "aws_instance" "ismail-1-ec2" {
    ami = data.aws_ami.ubuntu.id
    instance_type = "t2.micro"
    subnet_id = aws_subnet.ismail-1-subnet.id
    availability_zone = var.avail_zone[0]
    security_groups = [aws_security_group.ismail-ec2-sg.id]
    associate_public_ip_address = true
    tags = {
        Name: "ismail-1-ec2"
        source: "terraform"
    }
    key_name = "ismailkeyy"


} 

resource "aws_instance" "ismail-2-ec2" {
    ami = data.aws_ami.ubuntu.id
    instance_type = "t2.micro"
    subnet_id = aws_subnet.ismail-1-subnet.id
    availability_zone = var.avail_zone[0]
    security_groups = [aws_security_group.ismail-ec2-sg.id]
    associate_public_ip_address = true
    tags = {
        Name: "ismail-2-ec2"
        source: "terraform"
    }
     key_name = "ismailkeyy"


} 

resource "aws_instance" "ismail-3-ec2" {
    ami = data.aws_ami.ubuntu.id
    instance_type = "t2.micro"
    subnet_id = aws_subnet.ismail-2-subnet.id
    availability_zone = var.avail_zone[1]
    security_groups = [aws_security_group.ismail-ec2-sg.id]
    associate_public_ip_address = true
    tags = {
        Name: "ismail-3-ec2"
        source: "terraform"
    }
    key_name = "ismailkeyy"
} 

#exporting the ip adresses to the host inventory file
resource "local_file" "Ip_address" {
        filename = var.host_inventory_file
        content  = <<EOT
        ${aws_instance.ismail-1-ec2.public_ip}
        ${aws_instance.ismail-2-ec2.public_ip}
        ${aws_instance.ismail-3-ec2.public_ip}
     
        EOT
    }





/*resource "aws_lb" "ismail-lb" {
        name = "ismail-lb"
        internal = false
        load_balancer_type = "application"
        security_groups = [aws_security_group.ismail-lb-sg.id]
        subnets = [aws_subnet.ismail-1-subnet.id, aws_subnet.ismail-2-subnet.id ]
        enable_cross_zone_load_balancing = true
        enable_deletion_protection = false
        depends_on = [aws_instance.ismail-1-ec2, aws_instance.ismail-2-ec2, aws_instance.ismail-3-ec2]
}   

resource "aws_lb_target_group" "ismail-lb-tg" {
        name     = "ismail-lb-tg"
        target_type = "instance"
        port     = 80
        protocol = "HTTP"
        vpc_id   = aws_vpc.ismail-vpc.id
        health_check {
            path                = "/"
            protocol            = "HTTP"
            matcher             = "200"
            interval            = 15
            timeout             = 3
            healthy_threshold   = 3
            unhealthy_threshold = 3
        }
}

resource "aws_lb_listener" "ismail-listener" {
        load_balancer_arn = aws_lb.ismail-lb.arn
        port              = "80"
        protocol          = "HTTP"
        default_action {
            type             = "forward"
            target_group_arn = aws_lb_target_group.ismail-lb-tg.arn
        }
}
# Create the listener rule
resource "aws_lb_listener_rule" "ismail-listener-rule" {
        listener_arn = aws_lb_listener.ismail-listener.arn
        priority     = 1
        action {
            type             = "forward"
            target_group_arn = aws_lb_target_group.ismail-lb-tg.arn
        }
        condition {
            path_pattern {
            values = ["/"]
            }
        }


}*/




resource "aws_lb" "ismail-lb" {
        name = "ismail-lb"
        internal = false
        load_balancer_type = "application"
        security_groups = [aws_security_group.ismail-lb-sg.id]
        subnets = [aws_subnet.ismail-1-subnet.id, aws_subnet.ismail-2-subnet.id ]
        enable_cross_zone_load_balancing = true
        enable_deletion_protection = false
        depends_on = [aws_instance.ismail-1-ec2, aws_instance.ismail-2-ec2, aws_instance.ismail-3-ec2]
}   
    


resource "aws_lb_target_group" "ismail-lb-tg" {
        name     = "ismail-lb-tg"
        target_type = "instance"
        port     = 80
        protocol = "HTTP"
        vpc_id   = aws_vpc.ismail-vpc.id
        health_check {
            path                = "/"
            protocol            = "HTTP"
            matcher             = "200"
            interval            = 15
            timeout             = 3
            healthy_threshold   = 3
            unhealthy_threshold = 3
        }
}

resource "aws_lb_listener" "ismail-listener" {
        load_balancer_arn = aws_lb.ismail-lb.arn
        port              = "80"
        protocol          = "HTTP"
        default_action {
            type             = "forward"
            target_group_arn = aws_lb_target_group.ismail-lb-tg.arn
        }
}


# Create the listener rule
resource "aws_lb_listener_rule" "ismail-listener-rule" {
        listener_arn = aws_lb_listener.ismail-listener.arn
        priority     = 1
        action {
            type             = "forward"
            target_group_arn = aws_lb_target_group.ismail-lb-tg.arn
        }
        condition {
            path_pattern {
            values = ["/"]
            }
        }
}



 
#attachmentof the target groups
resource "aws_lb_target_group_attachment" "ismail-tg-attachment1" {
        target_group_arn = aws_lb_target_group.ismail-lb-tg.arn
        target_id        = aws_instance.ismail-1-ec2.id
        port             = 80
}
 
resource "aws_lb_target_group_attachment" "ismail-tg-attachment2" {
        target_group_arn = aws_lb_target_group.ismail-lb-tg.arn
        target_id        = aws_instance.ismail-2-ec2.id
        port             = 80
}
resource "aws_lb_target_group_attachment" "ismail-tg-attachment3" {
        target_group_arn = aws_lb_target_group.ismail-lb-tg.arn
        target_id        = aws_instance.ismail-3-ec2.id
        port             = 80 
}


# get hosted zone details
resource "aws_route53_zone" "hosted_zone" {
        name = var.domain_name
        tags = {
            Environment = "dev"
        }
}
# create a record set in route 53
# terraform aws route 53 record
resource "aws_route53_record" "site_domain" {
        zone_id = aws_route53_zone.hosted_zone.zone_id
        name    = "terraform-test.${var.domain_name}"
        type    = "A"
        alias {
            name                   = aws_lb.ismail-lb.dns_name
            zone_id                = aws_lb.ismail-lb.zone_id
            evaluate_target_health = true
        }
}

output public_ip {
  value       = aws_instance.ismail-3-ec2.public_ip
  
 
}







