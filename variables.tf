variable region {}
variable access_key {}
variable secret_key {}
variable vpc_cidr_block{}
variable public_subnet_cidr_blocks{}
variable avail_zone {}
variable host_inventory_file {}
variable "instance_ids" {
  type = list(string)
}
variable "instance_count" {
  default = 3
}
variable "domain_name" {
  default    = "ismailakanfe.me"
  type        = string
  description = "Domain name"
}

