# Terraform AWS Infrastructure

This Terraform code creates a Virtual Private Cloud (VPC) on AWS, along with the following resources:

## Resources
- Subnets
- Internet Gateway
- Route Table
- Network Access Control List
- Security Groups

## Requirements
- Terraform 0.12.x or higher
- AWS provider 4.0 or higher

## Usage
1. Clone the repository and navigate to the code directory
2. Initialize Terraform by running `terraform init`
3. Create a `terraform.tfvars` file and specify the following variables:
    - region: AWS region to deploy the infrastructure
    - access_key: AWS access key
    - secret_key: AWS secret key
    - vpc_cidr_block: CIDR block for the VPC
    - public_subnet_cidr_blocks: List of CIDR blocks for the public subnets
    - avail_zone: List of availability zones for the subnets
4. Run `terraform plan` to preview the changes
5. Apply the changes by running `terraform apply`

## Outputs
The following outputs will be displayed after Terraform is successfully applied:

- vpc_id: ID of the created VPC
- public_subnet_ids: IDs of the created public subnets
- route_table_id: ID of the created route table
- network_acl_id: ID of the created network access control list
- security_group_id: ID of the created security group

## Contributing
Feel free to contribute